package main

import (
	"bufio"
	"context"
	"flag"
	//"fmt"
	"log"
	"os"
	"strings"
	"text/template"

	"github.com/pkg/errors"
	tfe "github.com/hashicorp/go-tfe"
)

func main() {
	conf := getArgs()
	ctx := context.Background()
	tfc := configTF(conf)

	makeOrganization(ctx, tfc, conf)
	makeWorkspace(ctx, tfc, conf)
	writeFile(conf)
	updateDynamicSecrets(ctx, tfc, conf)

	updateTfvars(ctx, tfc, conf)
}

func makeOrganization(ctx context.Context, t *tfe.Client, c *config) {
	if c.Organization == "" {
		//use a random name, if toggle enabled
		panic("Blank org name is not handled")
	}

	orgs, err := t.Organizations.List(ctx, tfe.OrganizationListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	match := false
	orgname := normalize(c.Organization)
	// Look through org list
	for _, o := range orgs.Items {
		norm := normalize(o.Name)
		// case-insensitive and ignore trailing space
		if norm == orgname {
			match = true
			c.Organization = strings.TrimSpace(o.Name)
			break
		}
	}
	if !match {
		//exist? create if not
		orgname = strings.TrimSpace(c.Organization)
		opt := tfe.OrganizationCreateOptions{
			Name:  &orgname,
			Email: c.OrgEmail,
		}
		_, err := t.Organizations.Create(ctx, opt)
		if err != nil {
			//create failed
			panic(err)
		}
		log.Printf("Organization created: %s\n", c.Organization)
	}
}

func makeWorkspace(ctx context.Context, t *tfe.Client, c *config) {
	if c.Workspace == "" {
		//use a random name, if toggle enabled
		panic("Blank workspace name is not handled")
	}

	ws, err := t.Workspaces.List(ctx, c.Organization, tfe.WorkspaceListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	var wsitem *tfe.Workspace
	match := false
	wname := normalize(c.Workspace)

	for _, w := range ws.Items {
		norm := normalize(w.Name)
		if norm == wname {
			wsitem = w
			match = true
			c.Workspace = strings.TrimSpace(wsitem.Name)
			break
		}
	}
	if !match {
		//exist? create if not
		wname = strings.TrimSpace(c.Workspace)
		opt := tfe.WorkspaceCreateOptions{
			Name:             &wname,
			WorkingDirectory: c.WorkDir,
		}
		wsitem, err = t.Workspaces.Create(ctx, c.Organization, opt)
		if err != nil {
			//create failed
			panic(err)
		}
		log.Printf("Workspace created: %s\n", c.Workspace)
	}

	c.workspaceId = wsitem.ID
}

func updateDynamicSecrets(ctx context.Context, t *tfe.Client, c *config) {
	//todo fetch from TF Vault
	//     for now, accept values from cmd args

	vars, err := t.Variables.List(ctx, c.workspaceId, tfe.VariableListOptions{})
	if err != nil {
		log.Fatal(err)
	}

	aakdone := false
	askdone := false
	aregion := false
	// set dynamic secrets
	for _, v := range vars.Items {
		switch v.Key {
		case "AWS_ACCESS_KEY_ID":
			if c.AwsAccessKey != "" {
				aakdone = updateVar(ctx, t, c.workspaceId,
					v, c.AwsAccessKey, true)
			}

		case "AWS_SECRET_ACCESS_KEY":
			if c.AwsSecretKey != "" {
				askdone = updateVar(ctx, t, c.workspaceId,
					v, c.AwsSecretKey, true)
			}

		case "AWS_REGION":
			if c.AwsRegion != "" {
				aregion = updateVar(ctx, t, c.workspaceId,
					v, c.AwsRegion, false)
			}

		}
	}

	if !aakdone && c.AwsAccessKey != "" {
		createSecretVar(ctx, t, c.workspaceId,
			"AWS_ACCESS_KEY_ID", c.AwsAccessKey)
	}
	if !askdone && c.AwsSecretKey != "" {
		createSecretVar(ctx, t, c.workspaceId,
			"AWS_SECRET_ACCESS_KEY", c.AwsSecretKey)
	}
	if !aregion && c.AwsRegion != "" {
		createEnvVar(ctx, t, c.workspaceId,
			"AWS_REGION", c.AwsRegion)
	}
}

// TODO make sure not to use for sensitive variables
func updateTfvars(ctx context.Context, t *tfe.Client, c *config) {
	// read .tfvars file
	// then loop calling API var-update
	m := readTfvars(c)
	if len(m) == 0 {
		return
	}
	vars, err := t.Variables.List(ctx, c.workspaceId, tfe.VariableListOptions{})
	if err != nil {
		log.Fatal(errors.Wrap(err, "Variables.List failed"))
	}
	// loop to update
	for _, v := range vars.Items {
		if v.Category != tfe.CategoryTerraform {
			continue
		}
		if v.Sensitive {
			err := errors.New("Not-implemented changing sensitive var " + v.Key)
			log.Fatal(err)
		}
		targ := v.Key
		if item, ok := m[targ]; ok {
			//DEBUG DEBUG
			log.Printf("Updating... %s to be %s", targ, item.val)
			updateVar(ctx, t, c.workspaceId, v, item.val, v.Sensitive)
			item.done = true
			m[targ] = item
		}

	}
	// loop to create
	for key, item := range m {
		if item.done {
			continue
		}
		createInputVar(ctx, t, c.workspaceId, key, item.val)
	}
}

func updateVar(ctx context.Context, api *tfe.Client, ws string,
	tfv *tfe.Variable, val string, sen bool) bool {
	s := sen
	v := val
	k := tfv.Key
	opt := tfe.VariableUpdateOptions{
		Key:       &k,
		Value:     &v,
		Sensitive: &s,
	}
	_, err := api.Variables.Update(ctx, ws, tfv.ID, opt)
	if err != nil {
		log.Fatal(errors.Wrap(err, "Variables.Update"))
		return false
	}

	log.Printf("Var updated: %s", k)
	return true
}

func createSecretVar(ctx context.Context, api *tfe.Client, ws string,
	name string, val string) bool {
		return createVar(ctx, api, ws, name, val, true,
		tfe.CategoryEnv,
	);
}
func createEnvVar(ctx context.Context, api *tfe.Client, ws string,
	name string, val string) bool {
		return createVar(ctx, api, ws, name, val, false,
		tfe.CategoryEnv,
	);
}
func createInputVar(ctx context.Context, api *tfe.Client, ws string,
	name string, val string) bool {
		return createVar(ctx, api, ws, name, val, false,
		tfe.CategoryTerraform,
	);
}
func createVar(ctx context.Context, api *tfe.Client, ws string,
	name string, val string, sen bool, cat tfe.CategoryType) bool {
	s := sen
	v := val
	k := name
	c := cat
	opt := tfe.VariableCreateOptions{
		Key:       &k,
		Value:     &v,
		Sensitive: &s,
		Category:  &c,
	}
	_, err := api.Variables.Create(ctx, ws, opt)
	if err != nil {
		log.Fatal(errors.Wrap(err, "Variables.Create"))
		return false
	}

	log.Printf("Var created: %s", k)
	return true
}

func configTF(c *config) (t *tfe.Client) {
	tfconf := &tfe.Config{
		Token: c.TfcToken,
	}
	t, err := tfe.NewClient(tfconf)
	if err != nil {
		log.Fatal(errors.Wrap(err, "TF Cloud client failed from configuration"))
	}
	return
}

// TODO make sure not to use for sensitive variables
func readTfvars(c *config) (m map[string]tfvar) {
	m = make(map[string]tfvar)
	//loop each line
	//  split on equal sign
	//  and make left key, right val
	rdr, err := os.Open(c.TfvarsPath)
	if err != nil {
		log.Fatal(errors.Wrap(err, "File open failed on .tfvars path"))
	}
	defer rdr.Close()
	scanner := bufio.NewScanner(rdr)
	for scanner.Scan() {
		kv := strings.Split(scanner.Text(), "=")
		if len(kv) != 1 {
			k := strings.TrimSpace(kv[0])
			v := strings.TrimSpace(kv[1])
			m[k] = tfvar{false, v}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(errors.Wrap(err, "Scanner failed from reading .tfvars file"))
	}
	return
}

func writeFile(c *config) {
	log.Printf("Writing %s\n", c.BackendPath)
	// create will truncate existing file
	//todo check exists and rename
	f, err := os.Create(c.BackendPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	tmpl, err := template.New("test").Parse(bcktmpl)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(f, c)
	if err != nil {
		panic(err)
	}
}

func normalize(s string) string {
	// trim spaces and case-insensitive
	norm := strings.TrimSpace(s)
	return strings.ToLower(norm)
}

var bcktmpl = `
terraform {
  backend "remote" {
    organization = "{{.Organization}}"
    workspaces {
      name = "{{.Workspace}}"
    }
  }
}
`
type tfvar struct {
	done bool
	val string
}

type config struct {
	BackendPath  string
	Organization string
	OrgEmail     *string
	TfcToken     string
	TfvarsPath     string
	Workspace    string
	WorkDir      *string
	workspaceId  string
	AwsAccessKey string
	AwsSecretKey string
	AwsRegion    string
}

func getArgs() *config {
	tfc := flag.String("tfc", "", "Terraform Cloud API token")
	org := flag.String("org", "", "Organization name for Terraform Cloud")
	eml := flag.String("eml", "tester@example.com", "E-mail for organization")
	wrk := flag.String("wrk", "", "Workspace name for organization")
	dir := flag.String("dir", "tf", "Working directory for workspace")
	bck := flag.String("bck", "remote-backend.tf", "Path of backend block HCL file")
	aak := flag.String("aak", "", "The short-lived temp AWS access key")
	ask := flag.String("ask", "", "The short-lived temp AWS secret key")
	reg := flag.String("reg", "us-west-2", "AWS region")
	tfv := flag.String("tfv", "", "File path of .tfvars to import into TFC (DO NOT use for sensitive variables)")
	flag.Parse()

	return &config{
		TfcToken:     *tfc,
		TfvarsPath:     *tfv,
		Organization: *org,
		OrgEmail:     eml,
		Workspace:    *wrk,
		WorkDir:      dir,
		BackendPath:  *bck,
		AwsAccessKey: *aak,
		AwsSecretKey: *ask,
		AwsRegion:    *reg,
	}
}
