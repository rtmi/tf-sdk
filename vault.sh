#!/bin/sh

# non-production
# see learn.hashicorp.com/tutorials/vault/getting-started-dynamic-secrets?in=vault/getting-started
vault server -dev -dev-root-token-id=<dev-token-like-example-education>

# switch/open terminal for vault client
#######################################
vault secrets enable -path=aws aws

# NOT root account keys
export AWS_ACCESS_KEY_ID=<key-with-role-create-creds>
export AWS_SECRET_ACCESS_KEY=<secret-with-role-create-creds>

vault write aws/config/root \
	access_key=$AWS_ACCESS_KEY_ID \
	secret_key=$AWS_SECRET_ACCESS_KEY \
	region=us-west-2

# role of administrator (over-privilege) with TTL as compromise
vault write aws/roles/dev-role \
	policy_arns=arn:aws:iam::aws:policy/AdministratorAccess \
	credential_type=iam_user \
	lease_duration=20m
#iam_groups=Administrators

#	credential_type=assumed_role \
#	default_sts_ttl=15m \
#	max_sts_ttl=30m 
#role_arns=arn:aws:iam::515873892986:role/dynamicsecrets


# generate dynamic secrets
vault read aws/creds/dev-role

# revoke
#vault lease revoke <lease-id>

